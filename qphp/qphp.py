# -*- coding: utf-8 -*-
"""
    pygments.styles.qphp
    ~~~~~~~~~~~~~~~~~~~~~~

    The QPHP color style for pygments
"""

from pygments.style import Style
from pygments.token import Keyword, Name, Comment, String, Error, Number, Operator, Whitespace

class QPHPStyle(Style):
    default_style = ""

#    blue = '#137cbd'    
    navyblue = '#41708d'
    darkgreen = '#008a2c'
    black = '#000000'
    gold = '#d79a46'
    kobe = '#882d17'
    graphite = '#888888'
    red = '#aa0000'

    bold = 'bold'
    background = 'bg:'
    nobold = 'nobold'
    add = ' '
    
    styles = {
        Comment:                    darkgreen,
        Comment.Preproc:            navyblue,
        Keyword:                    bold + add + navyblue,
        Keyword.Constant:           nobold,
        Keyword.Pseudo:             nobold,

        Operator:                   black,
        Operator.Word:              bold + add + navyblue,
        Name:                       black,
        Name.Variable:              kobe,
        Name.Builtin.Pseudo:        kobe,
        Name.Label:                 red,
        
        String:                     graphite,
        Number:                     gold,
        
        Error:                      background + red,
    }

