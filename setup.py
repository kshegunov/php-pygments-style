""" 
A QPHP style for Pygments
""" 
from setuptools import setup 

setup( 
    name         = 'qphp', 
    version      = '1.0', 
    description  = __doc__, 
    author       = "Konstantin Shegunov", 
    install_requires = ['pygments'],
    packages     = ['qphp'], 
    entry_points = '''
    [pygments.styles]
    qphp = qphp:QPHPStyle
    '''
) 
